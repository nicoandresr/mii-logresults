import * as React from 'react'

function api() {
  const [users, setUsers] = React.useState()

  function fetchUsers(userName) {
     fetch(`https://api.github.com/users?q=${userName} in:login`)
       .then(response => response.json())
       .then(data => {
         setUsers(data)
     })
  }

  return { users, fetchUsers }
}

export default api
