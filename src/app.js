import * as React from 'react'

import useApi from './api'
import SearchForm from './components/search-form'

function App() {
  const { users, fetchUsers } = useApi()

  return (
    <>
      <SearchForm onSubmit={fetchUsers} />

      <ul
        className="grid grid-cols-4 gap-12 container mx-auto"
      >
        {users?.map(user => (
          <li
            key={user.id}
            className="bg-gray-100 rounded-xl shadow-xl"
          >
            <figure
              className="flex p-4"
            >
              <img
                className="rounded-full w-32 h-32 m-auto"
                src={user.avatar_url}
                alt={user.login}
                loading="lazy"
              />

              <figcaption
                className="flex flex-col items-center w-full mt-6"
              >
                <div className="capitalize text-3xl text-purple-500 font-bold">
                  {user.login}
                </div>
                <div className="text-sm text-gray-500">
                  {user.type}
                </div>
              </figcaption>
            </figure>
          </li>
        ))}
      </ul>
    </>
  )
}

export default App
