import * as React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

const defaultProps = {
}


function SearchForm({ onSubmit }) {
  function handleSubmit(event) {
    event.preventDefault()
    let [{ value: userName }] = event.target
    onSubmit(userName)
  }

  return ( 
    <form className="py-16 flex items-center justify-center" onSubmit={handleSubmit}>
      <input
        className="text-3xl p-4 font-sans bg-white rounded-xl w-1/2 h-16 shadow-xl"
        id="input"
      />

      <button
        type="submit"
        className="ml-6 bg-blue-600 w-32 text-3xl capitalize leading-loose rounded-xl shadow-xl text-gray-100"
      >
        submit
      </button>
    </form>
  )
}

SearchForm.propTypes = propTypes
SearchForm.defaultProps = defaultProps

export default SearchForm
