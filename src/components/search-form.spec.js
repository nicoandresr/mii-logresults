import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'

import SearchForm from './search-form'

test('Search form', () => {
  expect(render(
    <SearchForm
      onSubmit={() => {}}
    />
  )).toMatchSnapshot()
})
